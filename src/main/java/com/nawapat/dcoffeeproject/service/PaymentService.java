/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.PaymentDao;
import com.nawapat.dcoffeeproject.model.PaymentModel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author asus
 */
public class PaymentService {

    public List<PaymentModel> getPayments() {
        PaymentDao empDao = new PaymentDao();
        return empDao.getAll(" payment_id asc");
    }

    public PaymentModel addNew(PaymentModel editedPayment) {
        PaymentDao empDao = new PaymentDao();
        return empDao.save(editedPayment);
    }

    public PaymentModel update(PaymentModel editedPayment) {
        PaymentDao empDao = new PaymentDao();
        return empDao.update(editedPayment);
    }

    public int delete(PaymentModel editedPayment) {
        PaymentDao empDao = new PaymentDao();
        return empDao.delete(editedPayment);
    }
    
    public static String formatedDate(Date date) {
//        Date date = checkTime.getIn();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String formatedDate = df.format(date);
        return formatedDate;
    }
}
