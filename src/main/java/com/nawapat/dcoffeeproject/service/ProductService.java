/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.ProductDao;
import com.nawapat.dcoffeeproject.model.ProductModel;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ProductService {
//    private ProductDao productDao = new ProductDao();
//    public ArrayList<Product> getProductsOrderByName() {
//        return (ArrayList<Product>) productDao.getAll("product_name ASC ");
//    }
    
    public ProductModel getById(int id) {
        ProductDao productDao = new ProductDao();
        return productDao.get(id);
    }
            
    public List<ProductModel> getProducts(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" product_id asc");
    }
    
    public List<ProductModel> getProductsByCategory(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" product_category asc");
    }
    
    public List<ProductModel> getSearchByName1(String search){
        ProductDao productDao = new ProductDao();
        return productDao.getAllBySearch1(search);
    }
    
    public List<ProductModel> getSearchByName2(String search){
        ProductDao productDao = new ProductDao();
        return productDao.getAllBySearch2(search);
    }
    
    public List<ProductModel> getProductsByCategory1(){
        ProductDao productDao = new ProductDao();
        return productDao.getAllBywhere("product_category = 1");
    }
    
    public List<ProductModel> getProductsByCategory2(){
        ProductDao productDao = new ProductDao();
        return productDao.getAllBywhere("product_category = 2");
    }
    
    public List<ProductModel> getProductsByName(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" product_name asc");
    }

    public ProductModel addNew(ProductModel editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public ProductModel update(ProductModel editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(ProductModel editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }
}
