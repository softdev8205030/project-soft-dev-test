/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.EmployeeDao;
import com.nawapat.dcoffeeproject.model.EmployeeModel;
import java.util.List;

/**
 *
 * @author asus
 */
public class EmployeeService {

    public List<EmployeeModel> getEmployees() {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getAll(" employee_key asc");
    }

    public EmployeeModel addNew(EmployeeModel editedEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.save(editedEmployee);
    }

    public EmployeeModel update(EmployeeModel editedEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.update(editedEmployee);
    }

    public int delete(EmployeeModel editedEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.delete(editedEmployee);
    }
    
   public  String getName(int id) {
        EmployeeDao employeeDao = new EmployeeDao();
        EmployeeModel employee = employeeDao.get(id);

        return employee.getName();
    }
}
