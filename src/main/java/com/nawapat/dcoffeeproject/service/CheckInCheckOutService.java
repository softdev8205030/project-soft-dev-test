/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.CheckInCheckOutDao;
import com.nawapat.dcoffeeproject.model.CheckInCheckOutModel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author asus
 */
public class CheckInCheckOutService {
    public static CheckInCheckOutModel currentCheckInCheckOut;
    
    public CheckInCheckOutModel getCusById() {
        CheckInCheckOutDao inoutDao = new CheckInCheckOutDao();
        return inoutDao.get(1);
    }
    
    
    public List<CheckInCheckOutModel> getCheckInCheckOuts() {
        CheckInCheckOutDao inoutDao = new CheckInCheckOutDao();
        return inoutDao.getAll("id asc");
    }
    


    public CheckInCheckOutModel addNew(CheckInCheckOutModel editedCheckInCheckOut) {
        CheckInCheckOutDao inoutDao = new CheckInCheckOutDao();
        return inoutDao.save(editedCheckInCheckOut);
    }

    public CheckInCheckOutModel update(CheckInCheckOutModel editedCheckInCheckOut) {
        CheckInCheckOutDao inoutDao = new CheckInCheckOutDao();
        return inoutDao.update(editedCheckInCheckOut);
    }

    public int delete(CheckInCheckOutModel editedCheckInCheckOut) {
        CheckInCheckOutDao inoutDao = new CheckInCheckOutDao();
        return inoutDao.delete(editedCheckInCheckOut);
    }
    
//    wth?
//    public static void checkin() throws ParseException {
//        CheckInCheckOutDao checkinoutDao = new CheckInCheckOutDao();
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//        Date date = new Date();
//        Date dateOut = df.parse(df.format(date));
//        CheckInCheckOutModel checkinout = new CheckInCheckOutModel(date,dateOut,0,0,0,0);
//        checkinoutDao.save(checkinout);
//    }
}
