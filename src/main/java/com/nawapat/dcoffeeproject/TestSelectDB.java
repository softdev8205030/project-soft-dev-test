/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author parametsmac
 */
public class TestSelectDB {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffeeproject.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        // Select
        String sql = "SELECT s.salary_id, e.employee_key, e.employee_name, p.payment_date, p.payment_total, p.payment_status "
                + "FROM salary s "
                + "INNER JOIN employee e ON s.employee_id = e.employee_id "
                + "INNER JOIN payment p ON s.payment_id = p.payment_id";

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int salaryId = rs.getInt("salary_id");
                String employeeKey = rs.getString("employee_key");
                String employeeName = rs.getString("employee_name");
                String paymentDate = rs.getString("payment_date");
                double paymentTotal = rs.getDouble("payment_total");
                String paymentStatus = rs.getString("payment_status");

                System.out.println("Salary ID: " + salaryId);
                System.out.println("Employee Key: " + employeeKey);
                System.out.println("Employee Name: " + employeeName);
                System.out.println("Payment Date: " + paymentDate);
                System.out.println("Payment Total: " + paymentTotal);
                System.out.println("Payment Status: " + paymentStatus);
                System.out.println(); // เพิ่มบรรทัดว่างระหว่างแถว
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

//
////        //Select
//        String sql = "SELECT * FROM material";
//        try {
//            Statement stmt = conn.createStatement();
//            ResultSet rs = stmt.executeQuery(sql);
//
//            while (rs.next()) {
//                System.out.println(rs.getInt("material_id") + " "
//                        + rs.getString("material_key") + " " + rs.getString("material_name"));
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//
//        //Close
//        if (conn != null) {
//            try {
//                conn.close();
//            } catch (SQLException ex) {
//                System.out.println(ex.getMessage());
//            }
//        }
    }
}
