/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.CheckInCheckOutModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asus
 */
public class CheckInCheckOutDao implements Dao<CheckInCheckOutModel> {

    @Override
    public CheckInCheckOutModel get(int id) {
        CheckInCheckOutModel checkincheckout = null;
        String sql = "SELECT * FROM checkin_checkout WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkincheckout = CheckInCheckOutModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkincheckout;
    }


    public List<CheckInCheckOutModel> getAll() {
        ArrayList<CheckInCheckOutModel> list = new ArrayList();
        String sql = "SELECT * FROM checkin_checkout";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInCheckOutModel checkincheckout = CheckInCheckOutModel.fromRS(rs);
                list.add(checkincheckout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckInCheckOutModel> getAll(String where, String order) {
        ArrayList<CheckInCheckOutModel> list = new ArrayList();
        String sql = "SELECT * FROM checkin_checkout where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInCheckOutModel checkincheckout = CheckInCheckOutModel.fromRS(rs);
                list.add(checkincheckout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckInCheckOutModel> getAll(String order) {
        ArrayList<CheckInCheckOutModel> list = new ArrayList();
        String sql = "SELECT * FROM checkin_checkout  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInCheckOutModel checkincheckout = CheckInCheckOutModel.fromRS(rs);
                list.add(checkincheckout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckInCheckOutModel save(CheckInCheckOutModel obj) {

        String sql = "INSERT INTO checkin_checkout (duration, status, payment_id , user_id)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getDuration());
            stmt.setInt(2, obj.getStatus());
            stmt.setInt(3, obj.getPaymentid());
            stmt.setInt(4, obj.getUserid());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckInCheckOutModel update(CheckInCheckOutModel obj) {
        String sql = "UPDATE checkin_checkout"
                + " SET duration = ?, status = ?, payment_id = ?, user_id = ?"
                + " WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
             stmt.setInt(1, obj.getDuration());
            stmt.setInt(2, obj.getStatus());
            stmt.setInt(3, obj.getPaymentid());
            stmt.setInt(4, obj.getUserid());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckInCheckOutModel obj) {
        String sql = "DELETE FROM checkin_checkout WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
