/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.HistoryMatDetailModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class HistoryMatDetailDao implements Dao<HistoryMatDetailModel> {

    @Override
    public HistoryMatDetailModel get(int id) {
        HistoryMatDetailModel hisMatDetail = null;
        String sql = "SELECT * FROM history_material_detail WHERE history_material_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                hisMatDetail = HistoryMatDetailModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return hisMatDetail;
    }

    public List<HistoryMatDetailModel> getAll() {
        ArrayList<HistoryMatDetailModel> list = new ArrayList();
        String sql = "SELECT * FROM history_material_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HistoryMatDetailModel hisMatDetail = HistoryMatDetailModel.fromRS(rs);
                list.add(hisMatDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<HistoryMatDetailModel> getAll(String where, String order) {
        ArrayList<HistoryMatDetailModel> list = new ArrayList();
        String sql = "SELECT * FROM history_material_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HistoryMatDetailModel hisMatDetail = HistoryMatDetailModel.fromRS(rs);
                list.add(hisMatDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<HistoryMatDetailModel> getAll(String order) {
        ArrayList<HistoryMatDetailModel> list = new ArrayList();
        String sql = "SELECT * FROM history_material_detail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HistoryMatDetailModel hisMatDetail = HistoryMatDetailModel.fromRS(rs);
                list.add(hisMatDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<HistoryMatDetailModel> getAllById(int id) {
        ArrayList<HistoryMatDetailModel> list = new ArrayList();
        String sql = "SELECT * FROM history_material_detail WHERE history_material_id  =? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                HistoryMatDetailModel hisMatDetail = HistoryMatDetailModel.fromRS(rs);
                list.add(hisMatDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

//    @Override
//    public HistoryMatDetailModel save(HistoryMatDetailModel obj) {
//
//        String sql = "INSERT INTO history_material_detail (material_id, material_key, material_name, material_unit, material_remain, history_material_id)"
//                + "VALUES(?, ?, ?, ?, ?, ?)";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, obj.getMaterialId());
//            stmt.setString(2, obj.getMaterialKey());
//            stmt.setString(3, obj.getMaterialName());
//            stmt.setString(4, obj.getMaterialUnit());
//            stmt.setInt(5, obj.getMaterialRemain());
//            stmt.setInt(6, obj.getHistoryMaterialId());
////            System.out.println(stmt);
//            stmt.executeUpdate();
//            int id = DatabaseHelper.getInsertedId(stmt);
//            obj.setId(id);
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
//        return obj;
//    }
    @Override
public HistoryMatDetailModel save(HistoryMatDetailModel obj) {
    String insertSql = "INSERT INTO history_material_detail (material_id, material_key, material_name, material_unit, material_remain, history_material_id)"
            + "VALUES (?, ?, ?, ?, ?, ?)";
    String updateSql = "UPDATE material SET material_remain = ? WHERE material_id = ?";

    Connection conn = DatabaseHelper.getConnect();
    try {
        // ทำการ INSERT INTO ในตาราง history_material_detail
        try (PreparedStatement insertStmt = conn.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS)) {
            insertStmt.setInt(1, obj.getMaterialId());
            insertStmt.setString(2, obj.getMaterialKey());
            insertStmt.setString(3, obj.getMaterialName());
            insertStmt.setString(4, obj.getMaterialUnit());
            insertStmt.setInt(5, obj.getMaterialRemain());
            insertStmt.setInt(6, obj.getHistoryMaterialId());
            insertStmt.executeUpdate();

            // ดึงค่า id ที่ถูกเพิ่มเข้าไป
            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                int id = generatedKeys.getInt(1);
                obj.setId(id);
            } else {
                throw new SQLException("ไม่สามารถดึงค่า id ที่ถูกเพิ่มได้");
            }
        }

        // อัปเดตค่า material_remain ในตาราง material_table
        try (PreparedStatement updateStmt = conn.prepareStatement(updateSql)) {
            updateStmt.setInt(1, obj.getMaterialRemain());
            updateStmt.setInt(2, obj.getMaterialId());
            updateStmt.executeUpdate();
        }
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
        return null;
    } 
    return obj;
}
    

    @Override
    public HistoryMatDetailModel update(HistoryMatDetailModel obj) {
        String sql = "UPDATE history_material_detail"
                + " SET material_id = ?, material_key = ?, material_name = ?, material_unit = ?, material_remain = ?, history_material_id = ?"
                + " WHERE history_material_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMaterialId());
            stmt.setString(2, obj.getMaterialKey());
            stmt.setString(3, obj.getMaterialName());
            stmt.setString(4, obj.getMaterialUnit());
            stmt.setInt(5, obj.getMaterialRemain());
            stmt.setInt(6, obj.getHistoryMaterialId());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(HistoryMatDetailModel obj) {
        String sql = "DELETE FROM history_material_detail WHERE history_material_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
