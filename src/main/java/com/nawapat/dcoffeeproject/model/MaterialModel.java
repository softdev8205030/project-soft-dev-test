/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class MaterialModel {

    private int id;
    private String key;
    private String name;
    private float price;
    private String unit;
    private int min;
    private int remain;
    private int requestBuy;

    public MaterialModel(int id, String key, String name, float price, String unit, int min, int remain, int requestBuy) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.min = min;
        this.remain = remain;
        this.requestBuy = requestBuy;
    }

    public MaterialModel(String key, String name, float price, String unit, int min, int remain, int requestBuy) {
        this.id = -1;
        this.key = key;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.min = min;
        this.remain = remain;
        this.requestBuy = requestBuy;
    }

    public MaterialModel() {
        this.id = -1;
        this.key = "";
        this.name = "";
        this.price = 0;
        this.unit = "";
        this.min = 0;
        this.remain = 0;
        this.requestBuy = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public int getRequestBuy() {
        return requestBuy;
    }

    public void setRequestBuy(int requestBuy) {
        this.requestBuy = requestBuy;
    }

    @Override
    public String toString() {
        return "MaterialModel{" + "id=" + id + ", key=" + key + ", name=" + name + ", price=" + price + ", unit=" + unit + ", min=" + min + ", remain=" + remain + ", requestBuy=" + requestBuy + '}';
    }


    public static MaterialModel fromRS(ResultSet rs) {
        MaterialModel material = new MaterialModel();
        try {
            material.setId(rs.getInt("material_id"));
            material.setKey(rs.getString("material_key"));
            material.setName(rs.getString("material_name"));
            material.setPrice(rs.getFloat("material_price"));
            material.setUnit(rs.getString("material_unit"));
            material.setMin(rs.getInt("material_minimum"));
            material.setRemain(rs.getInt("material_remain"));
            material.setRequestBuy(rs.getInt("material_req"));
        } catch (SQLException ex) {
            Logger.getLogger(MaterialModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }

}
