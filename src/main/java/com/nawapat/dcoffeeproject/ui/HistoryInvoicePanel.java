/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.nawapat.dcoffeeproject.ui;

import com.nawapat.dcoffeeproject.dao.HistoryInvoiceDao;
import com.nawapat.dcoffeeproject.model.HistoryInvoiceDetailModel;
import com.nawapat.dcoffeeproject.model.HistoryInvoiceModel;
import com.nawapat.dcoffeeproject.model.MaterialModel;
import com.nawapat.dcoffeeproject.service.HistoryInvoiceDetailService;
import com.nawapat.dcoffeeproject.service.HistoryInvoiceService;
import com.nawapat.dcoffeeproject.service.MaterialService;
import com.nawapat.dcoffeeproject.service.UserService;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author asus
 */
public class HistoryInvoicePanel extends javax.swing.JPanel {

    private final JScrollPane scrMain;
    ArrayList<HistoryInvoiceModel> inList;
    private List<HistoryInvoiceDetailModel> invoiceList;
    private final HistoryInvoiceDetailService hisInvoiceDetailService;
    HistoryInvoiceService hisInvoiceService = new HistoryInvoiceService();
    AbstractTableModel tbHisDetailModel;
    HistoryInvoiceDao hisInvoiceDao = new HistoryInvoiceDao();
    UserService userService = new UserService();
    HistoryInvoiceModel hisInvoice;
    MaterialModel mat;
    MaterialService matService;

    public HistoryInvoicePanel(JScrollPane scrMain) {
        initComponents();
        initHisTable();
        this.scrMain = scrMain;
        hisInvoiceDetailService = new HistoryInvoiceDetailService();

        hisInvoiceDao = new HistoryInvoiceDao();
        hisInvoiceService = new HistoryInvoiceService();
        userService = new UserService();

        hisInvoice = new HistoryInvoiceModel();
        invoiceList = hisInvoiceDetailService.getHistoryInvoiceDetails();
        tbHisDetail.setRowHeight(30);
        tbHisDetailModel = new AbstractTableModel() {
            String[] hishead = {"Key", "Name", "Company", "Price", "Amount", "Total"};

            @Override
            public String getColumnName(int column) {
                return hishead[column];
            }

            @Override
            public int getRowCount() {
                return invoiceList.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                HistoryInvoiceDetailModel hisInDetailModel = invoiceList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return hisInDetailModel.getMaterialKey();
                    case 1:
                        return hisInDetailModel.getMaterialName();
                    case 2:
                        return hisInDetailModel.getMaterialCompany();
                    case 3:
                        return hisInDetailModel.getMaterialPrice();
                    case 4:
                        return hisInDetailModel.getMaterialQty();
                    case 5:
                        return hisInDetailModel.getMaterialTotal();
                    default:
                        return "";
                }
            }
        };
        showDate();
        showTime();
    }

    private void initHisTable() {
        inList = hisInvoiceService.getHisInvoices();
        tbHis.setRowHeight(30);
        tbHis.setModel(new AbstractTableModel() {
            String[] hishead = {"ID", "Employee", "Date", "Total"};

            @Override
            public String getColumnName(int column) {
                return hishead[column];
            }

            @Override
            public int getRowCount() {
                return inList.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                HistoryInvoiceModel hisInvoice = inList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return hisInvoice.getId();
                    case 1:
                        return hisInvoice.getUser().getEmployee().getName();
                    case 2:
                        return hisInvoice.getDate();
                    case 3:
                        return hisInvoice.getTotal();
                    default:
                        return "";
                }
            }

        }
        );
    }

    void showDate() {
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date d = new java.util.Date();
        Date.setText(s.format(d));
    }

    void showTime() {
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
                java.util.Date d = new java.util.Date();
                Time.setText(s.format(d));
            }
        }).start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        Time = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbHis = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbHisDetail = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 243, 228));

        jPanel3.setBackground(new java.awt.Color(255, 243, 228));
        jPanel3.setPreferredSize(new java.awt.Dimension(176, 69));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("History Invoice");

        Date.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Date.setForeground(new java.awt.Color(72, 52, 52));
        Date.setText("00/00/0000");

        Time.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Time.setForeground(new java.awt.Color(72, 52, 52));
        Time.setText("00.00.00 00");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(Date)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Time)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Date)
                    .addComponent(Time))
                .addContainerGap())
        );

        tbHis.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbHis.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "User", "Date", "Total"
            }
        ));
        tbHis.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbHisMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbHis);

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(238, 214, 196));

        btnAdd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelete)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnDelete))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tbHisDetail.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbHisDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Key", "Name", "Company", "Price", "Amount", "Total"
            }
        ));
        jScrollPane2.setViewportView(tbHisDetail);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 966, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 610, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 472, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 472, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnBack)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        scrMain.setViewportView(new MaterialPanel(scrMain));
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        scrMain.setViewportView(new AddInvoicePanel(scrMain));
    }//GEN-LAST:event_btnAddActionPerformed

    private void tbHisMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbHisMouseClicked
        int selectedIndex = tbHis.getSelectedRow();

        if (selectedIndex >= 0) {
            HistoryInvoiceModel him = inList.get(selectedIndex);

            invoiceList = hisInvoiceDetailService.getHistoryInvoiceDetailsById(him.getId());
            tbHisDetail.setModel(tbHisDetailModel);
            refreshHisTable(him.getId());
        }
    }//GEN-LAST:event_tbHisMouseClicked

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tbHis.getSelectedRow();
        if (selectedIndex >= 0) {
            hisInvoice = inList.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this,
                    "Do you want to Delete?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                hisInvoiceService.delete(hisInvoice);
            }
            refreshTable();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void refreshTable() {
        inList = hisInvoiceService.getHisInvoices();
        tbHis.revalidate();
        tbHis.repaint();
    }

    private void refreshHisTable(int id) {
        invoiceList = hisInvoiceDetailService.getHistoryInvoiceDetailsById(id);
//        lblTotal.setText("Total: " + hisInvoice.getTotal());
        tbHisDetail.revalidate();
        tbHisDetail.repaint();
    }

//    private void refreshRecipet(int rowIndex) {
//        HistoryInvoiceModel hisInvoice = inList.get(rowIndex);
//        lblTotal.setText("Total: " + hisInvoice.getTotal());
//        tbHisDetail.revalidate();
//        tbHisDetail.repaint();
//    }

    public JScrollPane getScrMain() {
        return scrMain;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Time;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDelete;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tbHis;
    private javax.swing.JTable tbHisDetail;
    // End of variables declaration//GEN-END:variables

}
